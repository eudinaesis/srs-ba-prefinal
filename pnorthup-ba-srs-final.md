---
title: "Spaced-Repetition Recall Apps in Secondary School Language Classes"
subtitle: "Possibilities and barriers to adoption"
author: Peter Northup
date: 25.01.2023 (Pre-final presentation)
lang: en
width: 1200
height: 800
revealjs-url: ../../../Developer/reveal.js
theme: white
bibliography: ./srs.json
nocite: |
    @*
---

# Background

## Formalities

- Title: "Spaced-Repetition Recall Apps in Secondary School Language Classes: Possibilities and Barriers to Adoption"
- Advisor: Renate Motschnig

## Context & Motivation

- There is a large and well-replicated evidence-base showing that recall exercises using "spaced repetition" (SR) aids memorization, especially over time
- Digitalization in schools and society means that every student can use SR apps on their own devices (e.g. apps/webapps on smartphones)
- ... but I believe these tools are not used very often, even in language classes where rote memorization is important and unavoidable

# Research question & approach

## Research question: why not?

1. Why don't (more) language teachers use these tools?
2. What would help overcome these barriers?

## Hypotheses

1. Unfamiliarity
   - Teachers unaware of the research
   - Teachers don't know appropriate apps
2. Policy/Data protection concerns
3. Too much effort, especially if doing it alone

## Methods

- Literature review: how confident can one be that SR is actually useful *in a school context*?
- Semi-structured interviews with a few language teachers in order to develop hypotheses further and guide potential solutions
- Develop instructional materials for teachers to
  - Very briefly summarize benefits of SR
  - Explain available apps/tools
  - Clarify issues with GDPR
- Larger online survey to assess
  - Reasons why teachers aren't using
  - Whether informational materials might help

# Plan

## Done

- [x] Literature search
- [x] Research GDPR issues
- [x] Assess available apps/services

## To Do

- [ ] Semi-structured interviews
- [ ] Write instructional materials
- [ ] Write survey for larger sample
- [ ] Send out survey
  - [ ] Facebook groups
  - [ ] #twlz: #twitterLehrerZimmer
- [ ] Evaluate results, write up paper

# Challenges & findings thus far

## Scientific support for SR

- SR is well-supported in *principle* and for tightly-controlled settings
- But very little reliable evidence that it can make a real difference in real-world school settings
  * "Absence of evidence" vs "evidence of absence" -- really hard to do a good study here!
  * What counts as the "treatment"? Making SR available? Requiring it? How much?
  * Not self-enforcing: if voluntary, very few use it [@seiberthansonEnhancingL2Learning2020]

## Implementation challenges

Few/no existing apps fulfill all requirements

  * Free: :x: Quizlet; :x: Anki iOS; :white_check_mark: Podsie
  * Ability for teacher to assign cards, assess progress: :white_check_mark: Quizlet; :white_check_mark: Podsie; :x: Anki; :x: lots of free apps/websites
  * GDPR compliance:
    - :x: lots of free ad-supported sites
    - :question: Quizlet
    - :question: :+1: Podsie

## GDPR compliance unclear

- I'm still not sure if even Podsie is compliant!
  - Nonprofit, designed for classes, has GDPR policies &c. -- but in USA!
- *Probably* ok if a teacher has class all use anonymized names... I think
- Really need clarity here!
- Still digesting 2023.01.17 EDPB report on use of cloud services by public sector

## Project challenges

- Social anxiety interfering with carrying out interviews
- General mental health, executive function (procrastination) challenges

## Rethinking scope/emphasis

May need to restructure the project

  - Drop the survey idea?
  - More thorough treatment of GDPR, practical implementation issues
  - Deeper engagement with literature
    * It's not as simple as "this works, why aren't teachers using it?"

# Thank you!

## Literature
