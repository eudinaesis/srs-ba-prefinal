# index.html is normal revealjs
# [filename].html is embed-resources revealjs
# .pdf is embed-resources revealjs
# make [all] does all 3, copies the media to public/media

all: public/index.html public/pnorthup-ba-srs-final.html public/pnorthup-ba-srs-final.pptx

public/index.html: pnorthup-ba-srs-final.md style.css
	pandoc --from markdown+emoji --standalone --citeproc --verbose --css style.css --embed-resources -t revealjs $< -o $@

public/pnorthup-ba-srs-final.html: pnorthup-ba-srs-final.md style.css
	pandoc --from markdown+emoji --standalone --citeproc --verbose --embed-resources $< -o $@

public/pnorthup-ba-srs-final.pptx: pnorthup-ba-srs-final.md style.css
	pandoc --standalone --citeproc --embed-resources $< -o $@
